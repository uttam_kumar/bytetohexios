//
//  ViewController.swift
//  ByteAndHexConversion
//
//  Created by Syncrhonous on 11/3/19.
//  Copyright © 2019 Syncrhonous. All rights reserved.
//

import UIKit
import Foundation



class ViewController: UIViewController {
    
    let hexString = "a5b7e534c45017b3cbbcea758c5fe11b"
    //let byteArray: [UInt8] =

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // hexstring to hex bytes
        let hexaBytes = hexString.hexa2Bytes
        print("hexaBytes: \(hexaBytes)")
        
        //bytes to hexString
        let hexaString = bytesConvertToHexstring(byte: hexaBytes)
        print("hexaStringFromByteArray: \(hexaString)")
    }
    
    
    //byte to hex
    func bytesConvertToHexstring(byte : [UInt8]) -> String {
        var string = ""
        for val in byte {
            //getBytes(&byte, range: NSMakeRange(i, 1))
            string = string + String(format: "%02X", val)
        }
        return string.lowercased()
    }
}



//hex to byte
extension StringProtocol {
    var hexa2Bytes: [UInt8] {
        var start = startIndex
        return stride(from: 0, to: count, by: 2).compactMap {  _ in
            let end = index(start, offsetBy: 2, limitedBy: endIndex) ?? endIndex
            defer { start = end }
            return UInt8(self[start..<end], radix: 16)
            //let string = String(bytes: chars, encoding: .utf8)
        }
    }
}

